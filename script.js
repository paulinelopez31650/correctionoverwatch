let hasError = false

const checkBattleTag = () => {         //fonction pour battletag
    const battletag= battletagInput.value
    const regex= /(^([A-zÀ-ú][A-zÀ-ú0-9]{2,11})|(^([а-яёА-ЯЁÀ-ú][а-яёА-ЯЁ0-9À-ú]{2,11})))(#[0-9]{4,})$/
    const error= document.getElementById("errorBattletag");
    hasError = !regex.test(battletag)
    error.style.display = hasError ? "block" : "none";
}

const submitForm = (e) =>{
    e.preventDefault()
    checkBattleTag();

    const battletag = battletagInput.value.replace('#', '-')
    const img = document.getElementById("icone")
    img.src = 'https://media.tenor.com/images/8d483e909ec3618f521e9700d6fbf2e1/tenor.gif';
    img.alt = "Chargement en cours";

    if (hasError) {
        img.src="https://miro.medium.com/max/978/1*pUEZd8z__1p-7ICIO1NZFA.png"
        img.alt = "Erreur";
        return 
    }

    fetch ('https://ow-api.com/v1/stats/pc/us/'+battletag+'/profile')
     .then(response => {
        if (response.ok) {
            return response.json()
        }
     })
     .then(data => {
         img.src=data.icon
         img.alt="Avatar de l'utilisateur " + data.name
     })
     .catch(() => {
        img.src="https://www.redactio.fr/wp-content/uploads/2018/10/erreur-404.png"
        img.alt = "Aucun utilisateur trouvé";

     })
 }


const battletagInput = document.getElementById("battletag"); //récupération input
battletagInput.addEventListener("change", checkBattleTag); //changement d'input

const form = document.getElementById("formulaire");
form.addEventListener("submit", submitForm) 




